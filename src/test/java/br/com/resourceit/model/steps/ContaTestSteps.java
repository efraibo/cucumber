package br.com.resourceit.model.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import br.com.resourceit.model.Conta;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ContaTestSteps {
	
	private Conta conta;
	  
    @Dado("^a conta criada para o dono \"(.*?)\" de numero (\\d+) com o limite (\\d+) e saldo (\\d+)$")
    public void a_conta_criada_para_o_dono_de_numero_com_o_limite_e_saldo(String dono, int numero, Double limite,
                 Double saldo) throws Throwable {
          // Definição de conta
          conta = new Conta(dono, numero, limite, saldo);
    }

    @Quando("^o dono realiza o deposito no valor de (\\d+) na conta$")
    public void o_dono_realiza_o_deposito_no_valor_de_na_conta(Double valorDeposito) throws Throwable {
          assertTrue("O dono " + conta.getDono() + " não tem limite disponível na conta para este valor de deposito",
                        conta.depositar(valorDeposito));
    }

    @E("^o dono realiza o primeiro saque no valor de (\\d+) na conta$")
    public void o_dono_realiza_o_primeiro_saque_no_valor_de_na_conta(Double valorSaque) throws Throwable {
          assertTrue("O dono " + conta.getDono() + " não tem saldo disponível na conta para este valor de saque",
                        conta.sacar(valorSaque));
    }

    @E("^o dono realiza o segundo saque no valor de (\\d+) na conta$")
    public void o_dono_realiza_o_segundo_saque_no_valor_de_na_conta(Double valorSaque) throws Throwable {
          assertTrue("O dono " + conta.getDono() + " não tem saldo disponível na conta para este valor de saque",
                        conta.sacar(valorSaque));
    }

    @Entao("^o dono tem o saldo no valor de (\\d+) na conta$")
    public void o_dono_tem_o_saldo_na_conta_no_valor_de(Double saldoEsperado) throws Throwable {
          assertEquals("O dono " + conta.getDono() + " está com o saldo incorreto na conta", saldoEsperado,
                        conta.getSaldo());
    }
    
    @Dado("^a conta criada para o dono \"(.*?)\" de numero (\\d+) com o limite (\\d+) com o saldo (\\d+) e situacao \"(.*?)\"$")
    public void a_conta_criada_para_o_dono_de_numero_com_o_limite_com_o_saldo_e_situacao(
    		String dono, int numero, Double limite, Double saldo, String situacao) throws Throwable {
    	conta = new Conta(dono, numero, limite, saldo, situacao);
    	
    }

    @Quando("^o dono criar a conta e depositar (\\d+) na conta$")
    public void o_dono_criar_a_conta_e_depositar_na_conta(Double valorDeposito) throws Throwable {
        assertTrue("O dono " + conta.getDono() + " cria uma conta e deposita um valor de abertura ",
        conta.depositar(valorDeposito));
    }
    
    @Entao("^A conta sera criada com sucesso e o saldo atualizado sera (\\d+)$")
    public void a_conta_sera_criada_com_sucesso_e_o_saldo_atualizado_sera(Double saldoEsperado) throws Throwable {
    	assertEquals("O dono " + conta.getDono() + " está com o saldo incorreto na conta", saldoEsperado,
              conta.getSaldo()); 
    }
    
}
