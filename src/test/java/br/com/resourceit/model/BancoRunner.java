package br.com.resourceit.model;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:stepDefinition", tags = "@BancoTeste", 
glue = "br.com.resourceit.model.steps", monochrome = true, dryRun = false)
public class BancoRunner {

}
